<?php

include 'components/connect.php';

if (isset($_GET['get_id'])) {

    $get_id = htmlspecialchars($_GET['get_id'], ENT_QUOTES, 'UTF-8');

} else {
    $get_id = '';
    header('location:all_posts.php');
}

if (isset($_POST['submit'])) {

    if ($user_id != '') {

        $id = create_unique_id();
        $title = filter_var($_POST['title'], FILTER_SANITIZE_STRING);
        $description = filter_var($_POST['description'], FILTER_SANITIZE_STRING);
        $rating = filter_var($_POST['rating'], FILTER_SANITIZE_STRING);

        $verify_review = $conn->prepare("SELECT * FROM `feedback` WHERE post_id = ? AND user_id = ?");
        $verify_review->execute([$get_id, $user_id]);

        if ($verify_review->rowCount() > 0) {
            $warning_msg[] = 'Your feedback already added!';
        } else {
            $add_review = $conn->prepare("INSERT INTO `feedback`(id, post_id, user_id, rating, title, description) VALUES(?,?,?,?,?,?)");
            $add_review->execute([$id, $get_id, $user_id, $rating, $title, $description]);
            $success_msg[] = 'Feedback added!';
        }
    } else {
        $warning_msg[] = 'Please login first!';
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>add review</title>

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

    <!-- header section starts  -->
    <?php include 'components/header.php'; ?>
    <!-- header section ends -->

    <!-- add review section starts  -->

    <section class="account-form">

        <form action="" method="post" onsubmit="return validateForm()">
            <h3>Post your Feedback</h3>
            <p class="placeholder">Feedback title <span>*</span></p>
            <input type="text" name="title" id="title" required maxlength="50" placeholder="enter feedback title"
                class="box">
            <p class="placeholder">Feedback description</p>
            <textarea name="description" id="description" class="box" placeholder="enter feedback description"
                maxlength="1000" cols="30" rows="10"></textarea>
            <p class="placeholder">rating <span>*</span></p>
            <select name="rating" id="rating" class="box" required>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
            </select>
            <input type="submit" value="submit Feedback" name="submit" class="btn">

            <a href="view_post.php?get_id=<?= htmlspecialchars($get_id, ENT_QUOTES, 'UTF-8'); ?>"
                class="option-btn">go back</a>

        </form>

        <script>
    function validateForm() {
        // Get form elements
        var titleInput = document.getElementById('title');
        var descriptionInput = document.getElementById('description');
        var ratingInput = document.getElementById('rating');

        // Validate title
        var titleValue = titleInput.value.trim();
        if (!titleValue) {
            alert('Please enter a feedback title.');
            return false;
        }
        // Check for special characters in the title
        if (/[^a-zA-Z0-9\s]/.test(titleValue)) {
            alert('Title should not include special characters.');
            return false;
        }

        // Validate description
        var descriptionValue = descriptionInput.value.trim();
        if (!descriptionValue) {
            alert('Please enter a feedback description.');
            return false;
        }
        // Check for special characters in the description
        if (/[^a-zA-Z0-9\s]/.test(descriptionValue)) {
            alert('Description should not include special characters.');
            return false;
        }

        // Validate rating
        var ratingValue = ratingInput.value.trim();
        if (!ratingValue) {
            alert('Please select a rating.');
            return false;
        }
        // Check for special characters in the rating (assuming it's a numeric input)
        if (/[^0-9]/.test(ratingValue)) {
            alert('Rating should be a number.');
            return false;
        }

        // If all validations pass, the form will be submitted.
        return true;
    }
</script>


    </section>

    <!-- add review section ends -->

    <!-- sweetalert cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <!-- custom js file link  -->
    <script type="module" src="js/script.js"></script>

    <?php include 'components/alers.php'; ?>

</body>

</html>
