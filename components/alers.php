<!DOCTYPE html>
<html>
<head>
    <!-- Include SweetAlert and your custom CSS -->
    <link rel="stylesheet" href="css/style.css">
    <!-- <link rel="stylesheet" href="path_to_custom.css"> -->
    <script src="js/script.js"></script>
</head>
<body>
<?php
if (isset($success_msg)) {
    foreach ($success_msg as $message) {
        echo '<script>swal({
            title: "' . $message . '",
            icon: "success",
            className: "success-swal"
        });</script>';
    }
}

if (isset($warning_msg)) {
    foreach ($warning_msg as $message) {
        echo '<script>swal({
            title: "' . $message . '",
            icon: "warning",
            className: "warning-swal"
        });</script>';
    }
}

if (isset($error_msg)) {
    foreach ($error_msg as $message) {
        echo '<script>swal({
            title: "' . $message . '",
            icon: "error",
            className: "error-swal"
        });</script>';
    }
}

if (isset($info_msg)) {
    foreach ($info_msg as $message) {
        echo '<script>swal({
            title: "' . $message . '",
            icon: "info",
            className: "info-swal"
        });</script>';
    }
}
?>
</body>
</html>


