<?php

$servername = 'localhost';
$db_name = 'securecoding_db';
$db_user_name = 'root';
$db_user_pass = '';

// Create a database connection using PDO
try {
   $conn = new PDO("mysql:host=$servername;dbname=$db_name", $db_user_name, $db_user_pass);
   $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
 
} catch (PDOException $e) {
   echo "Connection failed: " . $e->getMessage();
}

function create_unique_id() {
   $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
   $characters_length = strlen($characters);
   $random_string = '';
   for ($i = 0; $i < 20; $i++) {
      $random_string .= $characters[mt_rand(0, $characters_length - 1)];
   }
   return $random_string;
}

if (isset($_COOKIE['user_id'])) {
   $user_id = $_COOKIE['user_id'];
} else {
   $user_id = '';
}
?>
