<?php
session_start(); // Start the session to access session variables
include './components/connect.php';

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    // Collect the entered OTP from the form
    $enteredOtp = isset($_POST['otp']) ? (int)$_POST['otp'] : 0;
// Collect the entered email from the form
    // Check if the 'otp' index is set in the session
    if (isset($_SESSION['otp'])) {
        // Get the expected OTP from the session
        $expectedOtp = $_SESSION['otp'];

        // Compare the entered OTP with the one stored in the session
        if ($enteredOtp == $expectedOtp) {
            // Verification successful, perform necessary actions
            echo "<script>alert('OTP Successfully Verified'); window.location.href='http://localhost:8081/Nidup_securecoding_project/all_posts.php';</script>";

            // Clear the OTP from the session to avoid reuse
            unset($_SESSION['otp']);
        } else {
            // Verification failed
            echo "<script>alert('Sorry, Incorrect OTP! Try Again!'); window.location.href='http://localhost:8081/Nidup_securecoding_project/OTP.html';</script>";
        }
    } else {
        // Handle the case where 'otp' is not set in the session 
        echo "<script>alert('Please SignUp first'); window.location.href='http://localhost:8081/Nidup_securecoding_project/login.php';</script>";
    }
} else {
    // Handle the case where someone accesses this page without submitting the form
    echo "Invalid request.";
}
?>
