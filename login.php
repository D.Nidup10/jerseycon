<?php
session_start();
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// Include necessary PHPMailer files
require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\PHPMailer.php';
require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\SMTP.php';
require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\Exception.php';

// Include autoload file for additional dependencies
require 'PHPMailer-master/vendor/autoload.php';

// Include the database connection file
include 'components/connect.php';

// Check if the login form is submitted
if (isset($_POST['submit'])) {
    $email = filter_var($_POST['email'], FILTER_SANITIZE_EMAIL); // Sanitize and validate the email input
    $enteredPassword = $_POST['pass'];

// Validate email format
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        echo '<script>alert("Invalid email format."); window.location.href = "./login.php";</script>';
        exit();
    }

// Configures error reporting and sets a custom error handler function (customErrorHandler) to log errors.
    error_reporting(E_ALL);
    ini_set("display_errors", 0);

    function customErrorHandler($errno, $errstr, $errfile, $errline) {
        $timestamp = date('Y-m-d H:i:s');
        $message = "Error: [$errno] $errstr - $errfile: $errline";
        $logData = "$timestamp, $errno, $errstr, $errfile, $errline" . PHP_EOL;
        error_log($logData, 3, "error_log.csv"); // Log errors to a CSV file
    }

    set_error_handler("customErrorHandler");

    try {
        $query = $conn->prepare("SELECT * FROM users WHERE email = :email");  // Prepare a SELECT query to retrieve user information based on email

        if (!$query) {
            echo "Error: " . $conn->errorInfo()[2]; // Use $conn->errorInfo() for PDO
            exit;
        }
     // Bind the email parameter and execute the query
        $query->bindParam(':email', $email, PDO::PARAM_STR);
        $query->execute();

        $result = $query->fetch(PDO::FETCH_ASSOC);

        if (!empty($result)) {
            if ($result['verified'] == 1) {
                // Use password_verify to check if the entered password matches the stored hash
                if (password_verify($enteredPassword, $result['password'])) {
                    $fname = $result['name'];
                    $uid = $result['id'];
                    $email = $result['email'];
                    setcookie('user_id', $uid, time() + 60 * 60 * 24 * 30, '/');
                    $mail = new PHPMailer(true);
                    $mail->SMTPDebug = 2;
                    $mail->isSMTP();
                    $mail->SMTPAuth = true;
                    $mail->Host = 'smtp.gmail.com';
                    $mail->Username = '12210069.gcit@rub.edu.bt'; 
                    $mail->Password = 'svrrvfsncfdvhogp'; 
                    $mail->SMTPSecure = 'tls';
                    $mail->Port = 587; 
                    $mail->setFrom('12210069.gcit@rub.edu.bt', 'Your OTP');
                    $mail->addAddress($email);

                    $otp = rand(100000, 999999);
                    $_SESSION['otp'] = $otp;
                    $message = strval($otp);
                    $mail->Subject = "OTP";
                    $mail->Body = "Please enter the OTP we have sent in $email: $message";

                    if ($mail->send()) {
                        echo "<script>alert('Please enter the OTP we have sent to $email'); window.location.href='http://localhost:8081/Nidup_securecoding_project/OTP.html';</script>";
                    } else {
                        echo "Failed to send email. Error: " . $mail->ErrorInfo;
                    }
                } else {
                    $warning_msg[] = 'Incorrect password!';
                }
            } else {
                $warning_msg[] = 'User not verified! Please check your email for verification.';
            }
        } else {
            $errorMessage = "User not found! $email";
            customErrorHandler(E_USER_NOTICE, $errorMessage, FILE, __LINE__);
            $warning_msg[] = 'User not found!';
        }
    } catch (PDOException $e) {
        echo "Error: " . $e->getMessage();
        
    }
}
?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>login</title>

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">
</head>

<body>
    <!-- header section starts  -->
    <?php include 'components/header.php'; ?>
    <!-- header section ends -->

    <!-- login section starts  -->
    <section class="account-form">
        <form action="" method="post" enctype="multipart/form-data">
            <h3>Welcome!</h3>
            <p class="placeholder">Email Address <span>*</span></p>
            <input type="email" name="email" required maxlength="50" placeholder="enter your email" class="box">
            <p class="placeholder">Password <span>*</span></p>
            <input type="password" name="pass" required maxlength="50" placeholder="enter your password" class="box">
            <p class="link">Don't have an account? <a href="register.php">Register now</a></p>
            <input type="submit" value="login now" name="submit" class="btn">
        </form>
    </section>
    <!-- login section ends -->

    <!-- sweetalert cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <!-- custom js file link  -->
    <script src="js/script.js"></script>

    <?php include 'components/alers.php'; ?>
</body>

</html>
