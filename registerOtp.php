<?php
session_start();
$dbHost = 'localhost';
$dbUser = 'root';
$dbPassword = '';
$dbName = 'securecoding_db';

// Establish a database connection
$conn = mysqli_connect($dbHost, $dbUser, $dbPassword, $dbName);

// Check for connection errors
if (!$conn) {
    die("Connection failed: " . mysqli_connect_error());
}


if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if (isset($_SESSION['email'])) {

        $email = $_SESSION['email'];
        $enteredCode = isset($_POST['otp']) ? (int)$_POST['otp'] : 0;

        // Prepare and execute a query to retrieve the stored authentication code
        $sql = "SELECT verify_otp FROM users WHERE email = ?";
        $stmt = $conn->prepare($sql);
        $stmt->bind_param("s", $email);
        $stmt->execute();
        $stmt->bind_result($storedCode);
        $stmt->fetch(); 
        $stmt->close();
        if ($enteredCode == $storedCode) {
            $update_verified = $conn->prepare("UPDATE `users` SET verified = 1 WHERE email = ?");
            $update_verified->execute([$email]);        
            echo '<script>alert("Correctly entered the code"); window.location.href="login.php";</script>';
        } else {
            echo '<script>alert("Invalid code. Please try again."); window.location.href="V_OTP.html";</script>';
        }
    }
    else {
        echo "sorry";
    }
} else {
    // Handle the case when the request method is not POST
    echo "iam sorry";
}
?>