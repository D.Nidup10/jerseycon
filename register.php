<?php
session_start();

include 'components/connect.php';
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\PHPMailer.php';
require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\SMTP.php';
require 'D:\Xampp\htdocs\Nidup_securecoding_project\PHPMailer-master\src\Exception.php';

require 'PHPMailer-master/vendor/autoload.php';

// Function to generate CSRF token
function generateCSRFToken()
{
    return bin2hex(random_bytes(32));
}

// Initialize empty arrays to store error and success messages
$warning_msg = array();
$success_msg = array();


// Checks if the registration form has been submitted and validates the CSRF token.
if (isset($_POST['submit'])) {
    // Validate CSRF token 
    if (!isset($_POST['csrf_token']) || $_POST['csrf_token'] !== $_SESSION['csrf_token']) {
        die("CSRF Token Validation Failed!");
    }

    $id = create_unique_id();

    
    // Sanitize and validate inputs
    $name = filter_input(INPUT_POST, 'name', FILTER_SANITIZE_STRING);

    $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_EMAIL);

    $password = password_hash($_POST['pass'], PASSWORD_DEFAULT);

    $confirm_password = ($_POST['c_pass'] === $_POST['pass']);

    $image = filter_input(INPUT_POST, 'image', FILTER_SANITIZE_STRING);

    $_SESSION['email'] = $email; // Store the raw email in the session for backend processing

    // Validate and handle image upload
    $rename = '';
    if (!empty($image)) {
        $image_size = $_FILES['image']['size'];
        if ($image_size > 2000000) {
            $warning_msg[] = 'Image size is too large!';
        } else {
            $ext = pathinfo($_FILES['image']['name'], PATHINFO_EXTENSION);
            $rename = create_unique_id() . '.' . $ext;
            $image_folder = 'uploaded_files/' . $rename;
            move_uploaded_file($_FILES['image']['tmp_name'], $image_folder);
        }
    }

    // Check if email is already taken
    $verify_email = $conn->prepare("SELECT COUNT(*) as count FROM `users` WHERE email = ?");
    $verify_email->bindParam(1, $email); // Bind the parameter
    $verify_email->execute();
    

    $email_count = $verify_email->fetch(PDO::FETCH_ASSOC)['count'];
    $verify_otp = rand(100000, 999999);

    if ($email_count > 0) {
        $warning_msg[] = 'Email already taken!';        
    } else {
        if ($confirm_password) {
            // Insert user into the database
            $insert_user = $conn->prepare("INSERT INTO `users` (id, name, email, password, image, verify_otp, verified) VALUES (?, ?, ?, ?, ?, ?, 0)");
            $insert_user->execute([$id, $name, $email, $password, $rename, $verify_otp]);
            $mail = new PHPMailer(true);
            $mail->SMTPDebug = 2;
            $mail->isSMTP();
            $mail->SMTPAuth = true;
            $mail->Host = 'smtp.gmail.com';
            $mail->Username = '12210069.gcit@rub.edu.bt';
            $mail->Password = 'svrrvfsncfdvhogp';
            $mail->SMTPSecure = 'tls';
            $mail->Port = 587;
            $mail->setFrom('12210069.gcit@rub.edu.bt', 'Your OTP');
            $mail->addAddress($email);

            $message = $verify_otp;
            $mail->Subject = "OTP";
            $mail->Body = "Your OTP is: $message";

            if ($mail->send()) {
                echo "<script>alert('Registration successful! Please enter the OTP we have sent to " . htmlspecialchars($email, ENT_QUOTES, 'UTF-8') . " to verify it'); window.location.href='V_OTP.html';</script>";
            } else {
                echo "Failed to send email. Error: " . htmlspecialchars($mail->ErrorInfo, ENT_QUOTES, 'UTF-8');
            }
        } else {
            $warning_msg[] = 'Confirm password not matched!';
        }
    }
}

// Generate and store CSRF token in the session
$_SESSION['csrf_token'] = generateCSRFToken();

?>


<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>register</title>

    <!-- custom css file link  -->
    <link rel="stylesheet" href="css/style.css">

</head>

<body>

    <!-- header section starts  -->
    <?php include 'components/header.php'; ?>
    <!-- header section ends -->

    <section class="account-form">

        <form action="" method="post" enctype="multipart/form-data">
            <h3>make your account!</h3>

            <!-- Include CSRF token field -->
            <input type="hidden" name="csrf_token" value="<?php echo $_SESSION['csrf_token']; ?>">

            <p class="placeholder">Name <span>*</span></p>
            <input type="text" name="name" required maxlength="50" placeholder="enter your name" class="box">
            <p class="placeholder">Email <span>*</span></p>
            <input type="email" name="email" required maxlength="50" placeholder="enter your email" class="box">
            <p class="placeholder">Password <span>*</span></p>
            <input type="password" name="pass" required maxlength="50" placeholder="enter your password" class="box">
            <p class="placeholder">Confirm Password <span>*</span></p>
            <input type="password" name="c_pass" required maxlength="50" placeholder="confirm your password" class="box">
            <p class="placeholder">Profile pic</p>
            <input type="file" name="image" class="box" accept="image/*">
            <p class="link">Already have an account? <a href="login.php">login now</a></p>
            <input type="submit" value="register now" name="submit" class="btn">
        </form>

    </section>

    <script>
        document.addEventListener('DOMContentLoaded', function () {

            // Get form element
            var form = document.querySelector('form');

            // Add event listener for form submission
            form.addEventListener('submit', function (event) {

                // Validate name
                var nameInput = form.querySelector('input[name="name"]');
                var nameRegex = /^[a-zA-Z ]+$/;
                if (!nameInput.value.trim() || !nameRegex.test(nameInput.value)) {
                    alert('Please enter a valid name containing only alphabets.');
                    event.preventDefault();
                    return;
                }

                // Validate email
                var emailInput = form.querySelector('input[name="email"]');
                var emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
                if (!emailInput.value.trim() || !emailRegex.test(emailInput.value)) {
                    console.log('Please enter a valid email address.');
                    event.preventDefault();
                    return;
                }


                // Validate password
                var passwordInput = form.querySelector('input[name="pass"]');
                var passwordRegex = /^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$/;
                if (!passwordInput.value.trim() || !passwordRegex.test(passwordInput.value)) {
                    alert('Please enter a complex password with at least 8 characters, including uppercase, lowercase, numbers, and special characters.');
                    event.preventDefault();
                    return;
                }

                // Validate password confirmation
                var confirmPasswordInput = form.querySelector('input[name="c_pass"]');
                if (confirmPasswordInput.value.trim() !== passwordInput.value.trim()) {
                    alert('Passwords do not match.');
                    event.preventDefault();
                    return;
                }

                // You can add more validation for other fields if needed.

                // If all validations pass, the form will be submitted.
            });
        });
    </script>

    <!-- sweetalert cdn link  -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>

    <!-- custom js file link  -->
    <script src="js/script.js"></script>

    <?php include 'components/alers.php'; ?>

</body>

</html>
