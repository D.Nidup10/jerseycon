const profile = document.querySelector('.header .flex .profile');

document.querySelector('#user-btn').onclick = () => {
   profile.classList.toggle('active');
}

window.onscroll = () => {
   profile.classList.remove('active');
}

document.querySelectorAll('input[type="number"]').forEach(inputNumber => {
   inputNumber.addEventListener('input', () => {
      // Remove non-digit characters
      inputNumber.value = inputNumber.value.replace(/\D/g, '');
   });
});
