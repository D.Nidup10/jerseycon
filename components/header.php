<header class="header">
   <section class="flex">
      <a href="all_posts.php" class="logo"> <img src="logo.png" alt="Nidrup's online sports"></a></a>

      <nav class="navbar">
         <a href="all_posts.php" class="#">Home</a>
         <?php
            // Check if the user is logged in
            if ($user_id) {
         ?>
            <div id="user-btn" class="far fa-user"></div>
         <?php } else { ?>
            <a href="login.php" class="#">Login</a>
            <a href="register.php" class="#">Register</a>
         <?php } ?>
      </nav>

      <?php
         // Check if the user is logged in
         if ($user_id) {
            $select_profile = $conn->prepare("SELECT * FROM `users` WHERE id = ? LIMIT 1");
            $select_profile->execute([$user_id]);

            if ($select_profile->rowCount() > 0) {
               $fetch_profile = $select_profile->fetch(PDO::FETCH_ASSOC);
      ?>
               <div class="profile">
                  <?php if ($fetch_profile['image']) { ?>
                     <img src="uploaded_files/<?= $fetch_profile['image']; ?>" alt="" class="image">
                  <?php } ?>
                  <p><?= $fetch_profile['name']; ?></p>
                  <a href="update.php" class="btn">Update Profile</a>
                  <a href="components/logout.php" class="delete-btn" onclick="return confirm('Logout from this website?');">Logout</a>
               </div>
            <?php } else { ?>
               <div class="flex-btn">
                  <p>Please login or register!</p>
                  <a href="login.php" class="inline-option-btn">Login</a>
                  <a href="register.php" class="inline-option-btn">Register</a>
               </div>
            <?php }
         }
      ?>
   </section>
</header>
